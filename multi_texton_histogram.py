import math
import cv2
import numpy as np
import matplotlib.pyplot as plt



class MultiTexton():


	def __init__(self, image_path, CSA, CSB):
		self.CSA = CSA
		self.CSB = CSB
		self.image = cv2.imread(image_path)
		self.width, self.height, self.channels = self.image.shape

	def __init__(self, image, CSA, CSB):
		self.CSA = CSA
		self.CSB = CSB
		self.image = image
		self.width, self.height, self.channels = self.image.shape

	def get_ori(self):
		theta = self.texture_orientation_detection()
		ori = np.zeros(self.width * self.height).reshape(self.width, self.height)
		for i in range(0, self.width):
			for j in range(0, self.height):
				ori[i, j] = round(theta[i, j] * self.CSB / 180)

				if (ori[i, j] >= self.CSB - 1):
					ori[i, j] = self.CSB - 1

		return ori


	def texture_orientation_detection(self):
		arr = np.zeros(3 * self.width * self.height).reshape(self.width, self.height, 3)
		gxx = gyy = gxy = 0.0
		rh = gh = bh = 0.0
		rv = gv = bv = 0.0
		theta = np.zeros(self.width * self.height).reshape(self.width, self.height)

		for i in range(1, self.width - 2):
			for j in range(1, self.height - 2):
				rh = arr[i - 1, j + 1, 0] + 2 * arr[i, j + 1, 0] + arr[i + 1, j + 1, 0] - (
							arr[i - 1, j - 1, 0] + 2 * arr[i, j - 1, 0] + arr[i + 1, j - 1, 0])
				gh = arr[i - 1, j + 1, 1] + 2 * arr[i, j + 1, 1] + arr[i + 1, j + 1, 1] - (
							arr[i - 1, j - 1, 1] + 2 * arr[i, j - 1, 1] + arr[i + 1, j - 1, 1])
				bh = arr[i - 1, j + 1, 2] + 2 * arr[i, j + 1, 2] + arr[i + 1, j + 1, 2] - (
							arr[i - 1, j - 1, 2] + 2 * arr[i, j - 1, 2] + arr[i + 1, j - 1, 2])
				rv = arr[i + 1, j - 1, 0] + 2 * arr[i + 1, j, 0] + arr[i + 1, j + 1, 0] - (
							arr[i - 1, j - 1, 0] + 2 * arr[i - 1, j, 0] + arr[i - 1, j + 1, 0])
				gv = arr[i + 1, j - 1, 1] + 2 * arr[i + 1, j, 1] + arr[i + 1, j + 1, 1] - (
							arr[i - 1, j - 1, 1] + 2 * arr[i - 1, j, 1] + arr[i - 1, j + 1, 1])
				bv = arr[i + 1, j - 1, 2] + 2 * arr[i + 1, j, 2] + arr[i + 1, j + 1, 2] - (
							arr[i - 1, j - 1, 2] + 2 * arr[i - 1, j, 2] + arr[i - 1, j + 1, 2])

				gxx = math.sqrt(rh * rh + gh * gh + bh * bh)
				gyy = math.sqrt(rv * rv + gv * gv + bv * bv)
				gxy = rh * rv + gh * gv + bh * bv

				theta[i, j] = (math.acos(gxy / (gxx * gyy + 0.0001)) * 180 / math.pi)
		return theta

	def color_quantization(self):
		ImageX = np.zeros(self.width * self.height).reshape(self.width, self.height)
		R = G = B = 0
		VI = SI = HI = 0
		for i in range(0, self.width):
			for j in range(0, self.height):
				R = self.image[i, j][0]
				G = self.image[i, j][1]
				B = self.image[i, j][2]

				if (R >= 0 and R <= 64):
					VI = 0;
				if (R >= 65 and R <= 128):
					VI = 1;
				if (R >= 129 and R <= 192):
					VI = 2;
				if (R >= 193 and R <= 255):
					VI = 3;
				if (G >= 0 and G <= 64):
					SI = 0;
				if (G >= 65 and G <= 128):
					SI = 1;
				if (G >= 129 and G <= 192):
					SI = 2;
				if (G >= 193 and G <= 255):
					SI = 3;
				if (B >= 0 and B <= 64):
					HI = 0;
				if (B >= 65 and B <= 128):
					HI = 1;
				if (B >= 129 and B <= 192):
					HI = 2;
				if (B >= 193 and B <= 255):
					HI = 3;

				ImageX[i, j] = 16 * VI + 4 * SI + HI;
		return ImageX

	def texton_detection(self):
		ImageX = self.color_quantization()
		Texton = np.zeros(self.width * self.height).reshape(self.width, self.height)

		for i in range(0, (int)(self.width / 2)):
			for j in range(0, (int)(self.height / 2)):
				if (ImageX[2 * i, 2 * j] == ImageX[2 * i + 1, 2 * j + 1]):
					Texton[2 * i, 2 * j] = ImageX[2 * i, 2 * j];
					Texton[2 * i + 1, 2 * j] = ImageX[2 * i + 1, 2 * j];
					Texton[2 * i, 2 * j + 1] = ImageX[2 * i, 2 * j + 1];
					Texton[2 * i + 1, 2 * j + 1] = ImageX[2 * i + 1, 2 * j + 1];

				if (ImageX[2 * i, 2 * j + 1] == ImageX[2 * i + 1, 2 * j]):
					Texton[2 * i, 2 * j] = ImageX[2 * i, 2 * j];
					Texton[2 * i + 1, 2 * j] = ImageX[2 * i + 1, 2 * j];
					Texton[2 * i, 2 * j + 1] = ImageX[2 * i, 2 * j + 1];
					Texton[2 * i + 1, 2 * j + 1] = ImageX[2 * i + 1, 2 * j + 1];

				if (ImageX[2 * i, 2 * j] == ImageX[2 * i + 1, 2 * j]):
					Texton[2 * i, 2 * j] = ImageX[2 * i, 2 * j];
					Texton[2 * i + 1, 2 * j] = ImageX[2 * i + 1, 2 * j];
					Texton[2 * i, 2 * j + 1] = ImageX[2 * i, 2 * j + 1];
					Texton[2 * i + 1, 2 * j + 1] = ImageX[2 * i + 1, 2 * j + 1];

				if (ImageX[2 * i, 2 * j] == ImageX[2 * i, 2 * j + 1]):
					Texton[2 * i, 2 * j] = ImageX[2 * i, 2 * j];
					Texton[2 * i + 1, 2 * j] = ImageX[2 * i + 1, 2 * j];
					Texton[2 * i, 2 * j + 1] = ImageX[2 * i, 2 * j + 1];
					Texton[2 * i + 1, 2 * j + 1] = ImageX[2 * i + 1, 2 * j + 1];

		return Texton

	def make_multi_texton_histogram(self):
		MatrixH = np.zeros(self.CSA + self.CSB).reshape(self.CSA + self.CSB)
		MatrixV = np.zeros(self.CSA + self.CSB).reshape(self.CSA + self.CSB)
		MatrixRD = np.zeros(self.CSA + self.CSB).reshape(self.CSA + self.CSB)
		MatrixLD = np.zeros(self.CSA + self.CSB).reshape(self.CSA + self.CSB)

		ori = self.get_ori()
		Texton = self.texton_detection()

		D = 1  # distance parameter

		for i in range(0, self.width):
			for j in range(0, self.height - D):
				if (ori[i, j + D] == ori[i, j]):
					MatrixH[(int)(Texton[i, j])] += 1;
				if (Texton[i, j + D] == Texton[i, j]):
					MatrixH[(int)(self.CSA + ori[i, j])] += 1;

		for i in range(0, self.width - D):
			for j in range(0, self.height):
				if (ori[i + D, j] == ori[i, j]):
					MatrixV[(int)(Texton[i, j])] += 1;
				if (Texton[i + D, j] == Texton[i, j]):
					MatrixV[(int)(self.CSA + ori[i, j])] += 1;

		for i in range(0, self.width - D):
			for j in range(0, self.height - D):
				if (ori[i + D, j + D] == ori[i, j]):
					MatrixRD[(int)(Texton[i, j])] += 1;
				if (Texton[i + D, j + D] == Texton[i, j]):
					MatrixRD[(int)(self.CSA + ori[i, j])] += 1;

		for i in range(D, self.width):
			for j in range(0, self.height - D):
				if (ori[i - D, j + D] == ori[i, j]):
					MatrixLD[(int)(Texton[i, j])] += 1;
				if (Texton[i - D, j + D] == Texton[i, j]):
					MatrixLD[(int)(self.CSA + ori[i, j])] += 1;

		MTH = np.zeros(self.CSA + self.CSB).reshape(self.CSA + self.CSB)

		for i in range(0, self.CSA + self.CSB):
			MTH[i] = (MatrixH[i] + MatrixV[i] + MatrixRD[i] + MatrixLD[i]) / 4.0;

		return MTH

def main():
	mt = MultiTexton('test_image.jpg', CSA=64, CSB=18)
	hist = mt.make_multi_texton_histogram()

	plt.axis([0, 82, 0, 4000])
	plt.bar(np.arange(82), hist)
	plt.xlabel('Bin size')
	plt.ylabel('Frequency')
	plt.title('Histogram of MTH')
	plt.grid(True)

	plt.show()

if __name__ == '__main__':
    main()