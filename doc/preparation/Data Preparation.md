
# Building the data folders


```python
import os
import glob
import json
import shutil
```

## Sort data into train and test data

We will have to split the data into training and validation/test data. The following information written down in *json* Files in the */training_data/labels/* directory:
* **path**: '/home/user/Documents/data_science/image_segmentation/training_data/raw/***.jpg'
* **outputs**: \{*Label*\}
* **time_labeled**: ...
* **labeled**: {*True/False*}


```python
training_data_path = "/home/daniel/Documents/data_science/image_segmentation/training_data/"
label_path = "/home/daniel/Documents/data_science/image_segmentation/training_data/labels/"
```


```python
label_paths = glob.glob(label_path + "*.json")
```

Read the label files using the `glob` class. 


```python
json_labels = []

for p in label_paths:
    with open(p, 'r') as f:
        label_dict = json.load(f)
        json_labels.append(label_dict)
```

### Copy the files into respective directories

One sample label file


```python
json_labels[0]
```




    {'path': '/home/daniel/Documents/data_science/image_segmentation/training_data/raw/Q0TG5Y-23.jpg',
     'outputs': {},
     'time_labeled': 0,
     'labeled': False}




```python
os.path.join(training_data_path, 'train')
```




    '/home/daniel/Documents/data_science/image_segmentation/training_data/train'



Copy the images to their respective folders. We currently have two classes: **Foreground** and **Background**, since we want to first only segment damage and non-damage classes.  


```python
for l in json_labels:
    path = l['path']
    outputs = l['outputs']
    
    if len(outputs) > 0:
        name = outputs['object'][0]['name']
        
        if name == "Foreground":
            shutil.copy2(path, os.path.join(training_data_path, 'Foreground'))
        else:
            shutil.copy2(path, os.path.join(training_data_path, 'Background'))
    else:
        shutil.copy2(path, os.path.join(training_data_path, 'Background'))
```
