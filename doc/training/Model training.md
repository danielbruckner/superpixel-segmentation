
# Segment Classification

After labeling the images and putting them though the copy-script, we can now train a model on the images. There are many possibilities as to how we can represent the image and what type of model we can use to classify those representations. Since the original paper is a bit older, we change the approach to a more modern one. We use a CNN to classify the segments here. The original Paper used classical image processing features to basically do the same thing. If you want to further investigate this approach - Feel free to try different methods :)


```python
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.callbacks import EarlyStopping, ModelCheckpoint
```

## Data Generators

Classical Keras-based approach to input data from a directory using a Python-Generator.


```python
train_datagen = ImageDataGenerator(
        rotation_range=0,
        width_shift_range=0,
        height_shift_range=0,
        rescale=1./255,
        shear_range=0,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')
```


```python
test_datagen = ImageDataGenerator(rescale=1./255)
```


```python
batch_size = 32
```


```python
train_generator = train_datagen.flow_from_directory(
        './training_data/train',  
        target_size=(50, 50),  
        batch_size=batch_size,
        class_mode='binary')  

validation_generator = test_datagen.flow_from_directory(
        './training_data/val',
        target_size=(50, 50),
        batch_size=batch_size,
        class_mode='binary')
```

    Found 1400 images belonging to 2 classes.
    Found 600 images belonging to 2 classes.


##  Simple CNN Model

This is a very simple model, providing a very simple Proof of concept. If this classifier can find any signals, it is possible to use this approach. 


```python
model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=(50, 50, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten()) 
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))
```


```python
checkpoint = ModelCheckpoint('segment_classification.h5', monitor='val_loss', verbose=1, save_best_only=True)
early_stopping = EarlyStopping(monitor='val_loss', verbose=1, patience=10)
```


```python
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])
```


```python
history = model.fit_generator(
        train_generator,
        steps_per_epoch=1400 // batch_size,
        epochs=100,
        validation_data=validation_generator,
        validation_steps=600 // batch_size, 
    callbacks=[checkpoint, early_stopping])
```

    Epoch 1/100
    43/43 [==============================] - 4s 88ms/step - loss: 0.6484 - acc: 0.6456 - val_loss: 0.6831 - val_acc: 0.6406
    
    Epoch 00001: val_loss improved from inf to 0.68313, saving model to segment_classification.h5
    Epoch 2/100
    43/43 [==============================] - 4s 89ms/step - loss: 0.6341 - acc: 0.6601 - val_loss: 0.6505 - val_acc: 0.6637
    
    Epoch 00002: val_loss improved from 0.68313 to 0.65047, saving model to segment_classification.h5
    Epoch 3/100
    43/43 [==============================] - 4s 89ms/step - loss: 0.6100 - acc: 0.6718 - val_loss: 0.6330 - val_acc: 0.6831
    
    Epoch 00003: val_loss improved from 0.65047 to 0.63302, saving model to segment_classification.h5
    Epoch 4/100
    43/43 [==============================] - 4s 89ms/step - loss: 0.6105 - acc: 0.6759 - val_loss: 0.6738 - val_acc: 0.6056
    
    Epoch 00004: val_loss did not improve from 0.63302
    Epoch 5/100
    43/43 [==============================] - 4s 92ms/step - loss: 0.5975 - acc: 0.7013 - val_loss: 0.6023 - val_acc: 0.6778
    
    Epoch 00005: val_loss improved from 0.63302 to 0.60235, saving model to segment_classification.h5
    Epoch 6/100
    43/43 [==============================] - 4s 91ms/step - loss: 0.5665 - acc: 0.7382 - val_loss: 0.6223 - val_acc: 0.6989
    
    Epoch 00006: val_loss did not improve from 0.60235
    Epoch 7/100
    43/43 [==============================] - 4s 95ms/step - loss: 0.5619 - acc: 0.7296 - val_loss: 0.5576 - val_acc: 0.7254
    
    Epoch 00007: val_loss improved from 0.60235 to 0.55765, saving model to segment_classification.h5
    Epoch 8/100
    43/43 [==============================] - 4s 91ms/step - loss: 0.5235 - acc: 0.7677 - val_loss: 0.5500 - val_acc: 0.7254
    
    Epoch 00008: val_loss improved from 0.55765 to 0.55004, saving model to segment_classification.h5
    Epoch 9/100
    43/43 [==============================] - 4s 91ms/step - loss: 0.5432 - acc: 0.7350 - val_loss: 0.5992 - val_acc: 0.6831
    
    Epoch 00009: val_loss did not improve from 0.55004
    Epoch 10/100
    43/43 [==============================] - 4s 89ms/step - loss: 0.4991 - acc: 0.7812 - val_loss: 0.5931 - val_acc: 0.7218
    
    Epoch 00010: val_loss did not improve from 0.55004
    Epoch 11/100
    43/43 [==============================] - 4s 86ms/step - loss: 0.5229 - acc: 0.7524 - val_loss: 0.5348 - val_acc: 0.7306
    
    Epoch 00011: val_loss improved from 0.55004 to 0.53479, saving model to segment_classification.h5
    Epoch 12/100
    43/43 [==============================] - 4s 86ms/step - loss: 0.4957 - acc: 0.7587 - val_loss: 0.5353 - val_acc: 0.7465
    
    Epoch 00012: val_loss did not improve from 0.53479
    Epoch 13/100
    43/43 [==============================] - 4s 85ms/step - loss: 0.4671 - acc: 0.7965 - val_loss: 0.5325 - val_acc: 0.7482
    
    Epoch 00013: val_loss improved from 0.53479 to 0.53246, saving model to segment_classification.h5
    Epoch 14/100
    43/43 [==============================] - 4s 85ms/step - loss: 0.4869 - acc: 0.7701 - val_loss: 0.5122 - val_acc: 0.7588
    
    Epoch 00014: val_loss improved from 0.53246 to 0.51217, saving model to segment_classification.h5
    Epoch 15/100
    43/43 [==============================] - 4s 85ms/step - loss: 0.4584 - acc: 0.7875 - val_loss: 0.6023 - val_acc: 0.6954
    
    Epoch 00015: val_loss did not improve from 0.51217
    Epoch 16/100
    43/43 [==============================] - 4s 84ms/step - loss: 0.4732 - acc: 0.7835 - val_loss: 0.5162 - val_acc: 0.7412
    
    Epoch 00016: val_loss did not improve from 0.51217
    Epoch 17/100
    43/43 [==============================] - 4s 84ms/step - loss: 0.4722 - acc: 0.7965 - val_loss: 0.5148 - val_acc: 0.7606
    
    Epoch 00017: val_loss did not improve from 0.51217
    Epoch 18/100
    43/43 [==============================] - 4s 83ms/step - loss: 0.4646 - acc: 0.7812 - val_loss: 0.5214 - val_acc: 0.7482
    
    Epoch 00018: val_loss did not improve from 0.51217
    Epoch 19/100
    43/43 [==============================] - 3s 81ms/step - loss: 0.4480 - acc: 0.7955 - val_loss: 0.5591 - val_acc: 0.7377
    
    Epoch 00019: val_loss did not improve from 0.51217
    Epoch 20/100
    43/43 [==============================] - 4s 82ms/step - loss: 0.4435 - acc: 0.8140 - val_loss: 0.5042 - val_acc: 0.7674
    
    Epoch 00020: val_loss improved from 0.51217 to 0.50424, saving model to segment_classification.h5
    Epoch 21/100
    43/43 [==============================] - 4s 88ms/step - loss: 0.4336 - acc: 0.7943 - val_loss: 0.4880 - val_acc: 0.7764
    
    Epoch 00021: val_loss improved from 0.50424 to 0.48799, saving model to segment_classification.h5
    Epoch 22/100
    43/43 [==============================] - 4s 88ms/step - loss: 0.4375 - acc: 0.8043 - val_loss: 0.5018 - val_acc: 0.7623
    
    Epoch 00022: val_loss did not improve from 0.48799
    Epoch 23/100
    43/43 [==============================] - 4s 88ms/step - loss: 0.4406 - acc: 0.7890 - val_loss: 0.4474 - val_acc: 0.7905
    
    Epoch 00023: val_loss improved from 0.48799 to 0.44741, saving model to segment_classification.h5
    Epoch 24/100
    43/43 [==============================] - 4s 85ms/step - loss: 0.4181 - acc: 0.8190 - val_loss: 0.5625 - val_acc: 0.7412
    
    Epoch 00024: val_loss did not improve from 0.44741
    Epoch 25/100
    43/43 [==============================] - 4s 86ms/step - loss: 0.4317 - acc: 0.8060 - val_loss: 0.4704 - val_acc: 0.7799
    
    Epoch 00025: val_loss did not improve from 0.44741
    Epoch 26/100
    43/43 [==============================] - 4s 87ms/step - loss: 0.4525 - acc: 0.7955 - val_loss: 0.4801 - val_acc: 0.7764
    
    Epoch 00026: val_loss did not improve from 0.44741
    Epoch 27/100
    43/43 [==============================] - 4s 87ms/step - loss: 0.4066 - acc: 0.8103 - val_loss: 0.6163 - val_acc: 0.7535
    
    Epoch 00027: val_loss did not improve from 0.44741
    Epoch 28/100
    43/43 [==============================] - 4s 86ms/step - loss: 0.3993 - acc: 0.8232 - val_loss: 0.5546 - val_acc: 0.7746
    
    Epoch 00028: val_loss did not improve from 0.44741
    Epoch 29/100
    43/43 [==============================] - 4s 84ms/step - loss: 0.4196 - acc: 0.8084 - val_loss: 0.4864 - val_acc: 0.7940
    
    Epoch 00029: val_loss did not improve from 0.44741
    Epoch 30/100
    43/43 [==============================] - 4s 84ms/step - loss: 0.4209 - acc: 0.8096 - val_loss: 0.4941 - val_acc: 0.7746
    
    Epoch 00030: val_loss did not improve from 0.44741
    Epoch 31/100
    43/43 [==============================] - 4s 84ms/step - loss: 0.3990 - acc: 0.8258 - val_loss: 0.5134 - val_acc: 0.8028
    
    Epoch 00031: val_loss did not improve from 0.44741
    Epoch 32/100
    43/43 [==============================] - 4s 83ms/step - loss: 0.3870 - acc: 0.8307 - val_loss: 0.5402 - val_acc: 0.7641
    
    Epoch 00032: val_loss did not improve from 0.44741
    Epoch 33/100
    43/43 [==============================] - 4s 84ms/step - loss: 0.3916 - acc: 0.8307 - val_loss: 0.5934 - val_acc: 0.7430
    
    Epoch 00033: val_loss did not improve from 0.44741
    Epoch 00033: early stopping



```python
import matplotlib.pyplot as plt
```


```python
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
```




    [<matplotlib.lines.Line2D at 0x7fc7cb128518>]




![png](output_16_1.png)



```python
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
```




    [<matplotlib.lines.Line2D at 0x7fc7cb0df8d0>]




![png](output_17_1.png)


What we can see is that the classifier reched about 74% validation accuracy, which shows that it can learn something on the provided data. There are however a lot of improvements that have to be made for this model to work properly. Since we have only two classes, the baseline is 50%. So, the training process is visible, albeit improvable.
